class_name Entity extends Node2D

func _ready() -> void:
	for child in get_children():
		if child.has_method('_entity_ready'):
			child._entity_ready(self)
	assert(connect("child_entered_tree", self, '_on_child_entered_tree') == OK)

func _physics_process(delta: float) -> void:
	for child in get_children():
		if child.has_method('_entity_process'):
			child._entity_process(self, delta)

func _on_child_entered_tree(node: Node) -> void:
	if node.has_method('_entity_ready'):
		node._entity_ready(self)

func find_property(type: Script) -> Node:
	for child in get_children():
		if child.get_script() == type:
			return child
	return null

func connect_properties(
		emitter: Object,
		signal_name: String,
		prefix := ''
) -> void:
	var method_name := "_on_%s%s" % [prefix, signal_name]
	for child in get_children():
		if child.has_method(method_name):
			assert(emitter.connect(signal_name, child, method_name) == OK)
