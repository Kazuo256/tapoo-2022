class_name Destructible extends Node2D

signal destroyed(self_entity)

export var hit_points := 1
export var destroy_immediately := true

onready var max_hit_points := hit_points
onready var recovering := false

func _entity_ready(entity: Entity) -> void:
	entity.connect_properties(self, 'destroyed')

func _entity_process(entity: Entity, _delta: float) -> void:
	if hit_points == 0:
		emit_signal('destroyed', entity)
		if destroy_immediately:
			entity.queue_free()
	recovering = false

func take_hit(power: int) -> bool:
	if not recovering:
		hit_points = int(max(0, hit_points - power))
		recovering = true
		return true
	return false

func restore(amount: int) -> void:
	hit_points = int(min(hit_points + amount, max_hit_points))
