class_name InitialSpeed extends Node2D

export var initial_speed := Vector2.ZERO
export(float, 0.0, 180.0) var rotation_range := 0.0
export(float, 0.0, 0.5) var speed_range := 0.0

func _entity_ready(entity: Entity) -> void:
	var movement := entity.find_property(Movement) as Movement
	if movement:
		var angle := rand_range(-rotation_range, rotation_range)
		var speed_diff := rand_range(-speed_range, speed_range)
		var length := clamp(initial_speed.length() + speed_diff, 0.0, 1.0)
		var speed = initial_speed.rotated(deg2rad(angle)) * length
		movement.speed = speed
	queue_free()
