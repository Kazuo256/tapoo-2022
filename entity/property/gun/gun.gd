extends Position2D

signal spawned(ammo)

export var ammo_scn: PackedScene
export var fire_rate := 1.0

onready var cooldown := $Cooldown

func _ready() -> void:
	cooldown.wait_time = 1.0 / fire_rate

func _entity_process(entity: Entity, _delta: float) -> void:
	var shooter := entity.find_property(Shooter) as Shooter
	if shooter:
		if shooter.is_shooting and cooldown.is_stopped():
			cooldown.start()
			var ammo := ammo_scn.instance() as Entity
			ammo.position = global_position
			emit_signal("spawned", ammo)
			$ShootSFX.play()
