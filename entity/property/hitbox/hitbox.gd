class_name Hitbox extends Area2D

var entity: Entity

func _entity_ready(self_entity: Entity) -> void:
	entity = self_entity

func _entity_process(self_entity: Entity, _delta: float) -> void:
	for area in get_overlapping_areas():
		for child in get_children():
			if child.has_method('_on_hit'):
				child._on_hit(self_entity, area.entity)
