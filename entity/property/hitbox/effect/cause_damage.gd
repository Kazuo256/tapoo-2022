extends Node2D

signal spawned(spark)

export var power := 1
export var spark_scn: PackedScene
export var spark_range := 16

func _on_hit(self_entity: Entity, other_entity: Entity) -> void:
	var destructible := \
			other_entity.find_property(Destructible) as Destructible
	if destructible:
		if destructible.take_hit(power):
			var spark := spark_scn.instance()
			spark.position = (
					self_entity.global_position +
					other_entity.global_position
			) / 2
			spark.position += Vector2(
					rand_range(-spark_range, spark_range),
					rand_range(-spark_range, spark_range)
			)
			emit_signal("spawned", spark)
