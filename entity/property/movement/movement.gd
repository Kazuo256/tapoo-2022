class_name Movement extends Node2D

export var max_speed := 500
export var damp_factor := 0.9

onready var speed := Vector2.ZERO
onready var last_collision: KinematicCollision2D = null

func _entity_process(entity: Entity, delta: float) -> void:
	speed = speed.limit_length(1.0)
	entity.position += speed * max_speed * delta
	speed *= damp_factor
