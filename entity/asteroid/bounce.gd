extends Node2D

func _on_hit(self_entity: Entity, other_entity: Entity) -> void:
	var movement := self_entity.find_property(Movement) as Movement
	if movement:
		var diff := self_entity.position - other_entity.position
		var length := movement.speed.length()
		var aligned := sign(movement.speed.dot(diff))
		var new_speed = aligned * movement.speed.reflect(diff.normalized())
		movement.speed = new_speed.normalized() * length
