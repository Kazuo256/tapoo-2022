extends Sprite

export var rotation_speed := 180.0

func _process(delta: float) -> void:
	rotation_degrees += rotation_speed * delta
