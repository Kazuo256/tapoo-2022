extends Node2D

signal spawned(part)

export var part_scn: PackedScene
export var amount := 3
export var offset := 24

func _on_destroyed(entity: Entity) -> void:
	if part_scn == null:
		return
	var dir
	var movement := entity.find_property(Movement) as Movement
	if movement:
		dir = movement.speed
	for _i in amount:
		var part := part_scn.instance()
		part.position = global_position + Vector2(
				rand_range(-offset, offset),
				rand_range(-offset, offset)
		)
		if part is Entity:
			var initial_speed := part.find_property(InitialSpeed) as InitialSpeed
			if initial_speed and dir:
				initial_speed.initial_speed = dir
		emit_signal("spawned", part)
