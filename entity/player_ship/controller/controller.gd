class_name Controller extends Node

func _entity_process(entity: Entity, _delta: float) -> void:
	var movement := entity.find_property(Movement) as Movement
	if movement:
		var horizontal = Input.get_action_strength("move_right") \
					   - Input.get_action_strength("move_left")
		var vertical = Input.get_action_strength("move_down") \
					 - Input.get_action_strength("move_up")
		var motion = Vector2(horizontal, vertical)
		if motion.length_squared() > movement.speed.length_squared():
			movement.speed = motion
	var shooter := entity.find_property(Shooter) as Shooter
	if shooter:
		shooter.is_shooting = Input.is_action_pressed("shoot")
