extends Sprite

export var amplitude := 8
export var frequency := 1.5

onready var original_position := position
onready var phase := rand_range(0.0, 1.0)

func _process(delta: float) -> void:
	phase += delta
	phase = fmod(phase, 1 / frequency)
	var osc = amplitude * sin(frequency * phase * TAU)
	position.y = original_position.y + osc
