extends Node2D

export var power := 50
export var vfx_scn: PackedScene

func _on_hit(self_entity: Entity, other_entity: Entity) -> void:
	var destructible := other_entity.find_property(Destructible) as Destructible
	if destructible:
		destructible.restore(power)
		self_entity.queue_free()
		var vfx := vfx_scn.instance()
		other_entity.add_child(vfx)
