
## Palette

https://coolors.co/7d8491-443850-ba5a31-ffc9b5-f2efea

## Outline

Title: Desenvolvimento de Jogos e OO (com Godot!)

* Jogos são simulações interativas em tempo-real
  - Jogo = Simulação + I/O + progressão temporal
  - Simulação = elementos estáticos + elementos dinâmicos
  - Elementos dinâmicos -> objetos
* Abstração central: Nós e Árvores
  - Padrão *Composite*
  - Composição de transformações afins
  - Nós possuem hierarquia
  - Exemplo 01: label de hello world
* Organização de projeto: Cenas
  - Princício *Data-Driven Design*
    + Cenas são a configuração inicial de Árvores persistidas
  - Padrão *Prototype*
    + Cenas são instanciáveis, permitindo composição
    + Exemplo 02: instanciar asteroides
    + Cenas são herdáveis, permitindo extensão
    + Exemplo 03: asteroide grande
    + Exemplo 04: stages
    + Cenas são... objetos!!
    + Exemplo 05: nave do jogador
* Comportamento dinâmico: Scripts
  - Padrão *Strategy* (ou *Update*) e *Component*
  - Scripts implementam como Nós agem na simulação
  - Scripts são classes
  - Exemplo 06: entidade + movimento da nave do jogador
  - Exemplo 07: label com informação de debug
* Comunicação entre objetos: Sinais
  - Padrão *Observer*
  - Permite programação assíncrona
  - Exemplo 08: spawn de asteroides
  - Exemplo 09: inimigo que atira
* Discussão
  - Importância de ter objetos
    - Persistir estado entre frames
    - Encapsulamento
    - Reuso através da composição
  - Problemas de ter objetos
    - Encapsulamento (!!)
    - Ordem de execução (determinismo)
    - Otimização 1: difícil de paralelizar
    - Otimização 2: localidade
  - Alternativas: padrão *Entity-Component-System*

## TODO

* [ ] Asteroid
* [ ] Simple Enemy (moves straight and slow, shoots at a very low rate)
* [ ] Spawner
* [ ] Stage
* [ ] Stage design
