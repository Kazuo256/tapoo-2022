extends Node2D

func _ready() -> void:
	_connect_spawn_signal_r(self)
	assert(get_tree().connect(
			"node_added", self, '_connect_spawn_signal'
	) == OK)

func _connect_spawn_signal(node: Node) -> void:
	if node.has_signal('spawned'):
		assert(node.connect('spawned', self, '_on_spawned') == OK)

func _connect_spawn_signal_r(node: Node) -> void:
	_connect_spawn_signal(node)
	for child in node.get_children():
		_connect_spawn_signal_r(child)

func _on_spawned(node: Node) -> void:
	add_child(node)
