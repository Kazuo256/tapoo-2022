extends Sprite

export var speed := 100

func _process(delta: float) -> void:
	region_rect.position.y -= speed * delta
	var h = texture.get_height()
	region_rect.position.y = fmod(region_rect.position.y, h)
