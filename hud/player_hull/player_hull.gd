extends TextureProgress

export var player_ship_path: NodePath
export var smooth_factor := 10.0

onready var player_ship := get_node_or_null(player_ship_path) as Entity
onready var target_value := 100.0

func _process(delta: float) -> void:
	if is_instance_valid(player_ship):
		var destructible := player_ship.find_property(Destructible) as Destructible
		target_value = destructible.hit_points
	else:
		target_value = 0
	var diff := target_value - value
	var shift := diff * smooth_factor * delta
	if shift*shift > step:
		value += shift
	else:
		value = target_value
